package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

public class VersementMapper {

    private static VersementDto virementDto;

    public static VersementDto map(Versement versement) {
        virementDto = new VersementDto();
        //virementDto.setNrCompteEmetteur(versement.getEmetteur().getNrCompte());
        virementDto.setDate(versement.getDateExecution());
        virementDto.setMotif(versement.getMotifVersement());

        return virementDto;

    }

}
