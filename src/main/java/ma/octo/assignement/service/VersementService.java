package ma.octo.assignement.service;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;

@Service
@Transactional
public class VersementService {
	@Autowired
	private CompteRepository compterepository;
	Logger LOGGER = LoggerFactory.getLogger(VirementService.class);
	@Autowired
	private VersementRepository versementrepository;
	public static final int MONTANT_MAXIMAL = 10000;

	public void checVersement(VersementDto versementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        Compte f12 = compterepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());
       
        if (f12 == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (versementDto.getMontantVersement().equals(null)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (versementDto.getMotif().length() < 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

       

        f12
                .setSolde(new BigDecimal(f12.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compterepository.save(f12);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDate());
        versement.setCompteBeneficiaire(f12);
        
        versement.setMontantVersement(versementDto.getMontantVersement());

        versementrepository.save(versement);
     
	}
}
