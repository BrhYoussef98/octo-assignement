package ma.octo.assignement.service;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;

@Service
@Transactional
public class VirementService {
	@Autowired
	private CompteRepository compterepository;
	Logger LOGGER = LoggerFactory.getLogger(VirementService.class);
	@Autowired
	private VirementRepository virementrepository;
	public static final int MONTANT_MAXIMAL = 10000;

	public void checVirement(VirementDto virementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		Compte c1 = compterepository.findByNrCompte(virementDto.getNrCompteEmetteur());
		Compte f12 = compterepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

		if (c1 == null) {
			System.out.println("Compte Non existant");
			throw new CompteNonExistantException("Compte Non existant");
		}

		if (f12 == null) {
			System.out.println("Compte Non existant");
			throw new CompteNonExistantException("Compte Non existant");
		}

		if (virementDto.getMontantVirement().equals(null)) {
			System.out.println("Montant vide");
			throw new TransactionException("Montant vide");
		} else if (virementDto.getMontantVirement().intValue() == 0) {
			System.out.println("Montant vide");
			throw new TransactionException("Montant vide");
		} else if (virementDto.getMontantVirement().intValue() < 10) {
			System.out.println("Montant minimal de virement non atteint");
			throw new TransactionException("Montant minimal de virement non atteint");
		} else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
			System.out.println("Montant maximal de virement dépassé");
			throw new TransactionException("Montant maximal de virement dépassé");
		}

		if (virementDto.getMotif().length() < 0) {
			System.out.println("Motif vide");
			throw new TransactionException("Motif vide");
		}

		if (c1.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
			LOGGER.error("Solde insuffisant pour l'utilisateur");
		}

		if (c1.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
			LOGGER.error("Solde insuffisant pour l'utilisateur");
		}

		c1.setSolde(c1.getSolde().subtract(virementDto.getMontantVirement()));
		compterepository.save(c1);

		f12.setSolde(new BigDecimal(f12.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
		compterepository.save(f12);

		Virement virement = new Virement();
		virement.setDateExecution(virementDto.getDate());
		virement.setCompteBeneficiaire(f12);
		virement.setCompteEmetteur(c1);
		virement.setMontantVirement(virementDto.getMontantVirement());
		virementrepository.save(virement);

	}
}
