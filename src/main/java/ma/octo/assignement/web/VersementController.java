package ma.octo.assignement.web;
import ma.octo.assignement.domain.Compte;

import ma.octo.assignement.domain.Versement;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;

import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirementService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController(value = "/versement")
public class VersementController {
    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    private CompteRepository compterepository;
    @Autowired
    private VersementRepository versementrepository;
    @Autowired
    private AutiService autiservice;
    @Autowired
    private UtilisateurRepository utilisateurrepository;
    @Autowired
    private VersementService versementservice;


    
    @PostMapping("/executerversement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VersementDto versementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
    	versementservice.checVersement(versementDto);
        autiservice.auditVersement("Versement depuis " + versementDto.getnmEmetteur() + " vers " + versementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontantVersement()
                .toString());
        
    }
    private void save(Versement Versement) {
        versementrepository.save(Versement);
    }

}
