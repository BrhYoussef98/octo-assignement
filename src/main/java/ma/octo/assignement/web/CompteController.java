package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;

@RestController(value = "/comptes")
public class CompteController {
	  @Autowired
	    private CompteRepository compterepository;
	 @GetMapping("lister_comptes")
	    List<Compte> loadAllCompte() {
	        List<Compte> all = compterepository.findAll();

	        if (CollectionUtils.isEmpty(all)) {
	            return null;
	        } else {
	            return all;
	        }
	    }
}
